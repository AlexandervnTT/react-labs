import React, { Component } from "react";

import { Router, Route } from "./components/Router";

import { Menu } from "./components/Menu";

import { Dashboard } from "./containers/Dashboard";
import { ScrollToExample } from "./containers/ScrollToExample";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <Menu />

                    <Route path="/">
                        <Dashboard />
                    </Route>
                    <Route path="/scroll-to-example">
                        <ScrollToExample />
                    </Route>
                </Router>
            </div>
        );
    }
}

export default App;
