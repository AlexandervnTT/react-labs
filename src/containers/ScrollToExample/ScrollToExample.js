import React from "react";
import { ScrollTo } from "../../components/ScrollTo";
import { LoremIpsum } from "../../components/LoremIpsum";

class ScrollToExample extends React.Component {
    render() {
        return (
            <React.Fragment>
                <ScrollTo>
                    {scrollTo => (
                        <React.Fragment>
                            <button
                                onClick={() => {
                                    scrollTo(this.span);
                                }}
                            >
                                Scroll
                            </button>
                            <LoremIpsum />
                            <span ref={span => (this.span = span)}>Span</span>
                        </React.Fragment>
                    )}
                </ScrollTo>
            </React.Fragment>
        );
    }
}

export default ScrollToExample;
