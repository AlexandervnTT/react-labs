import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import Text from "./Text";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Text />, div);
});

it("renders correctly", () => {
    const tree = renderer.create(<Text>Disco</Text>).toJSON();
    expect(tree).toMatchSnapshot();
});
