import React from "react";

function Text(props) {
    return <span>{props.children}</span>;
}

export default Text;
