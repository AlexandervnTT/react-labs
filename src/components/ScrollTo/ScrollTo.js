import React from "react";

class ScrollTo extends React.Component {
    scrollTo(data) {
        if (data instanceof Element) {
            data = data.getBoundingClientRect();
        }

        window.scroll(data.x, data.y);
    }

    render() {
        return (
            <div>
                {this.props.children &&
                    this.props.children(this.scrollTo.bind(this))}
            </div>
        );
    }
}

export default ScrollTo;
