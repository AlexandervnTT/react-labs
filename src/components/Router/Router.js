import React from "react";

import { LocationStore } from "./";

class Router extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <LocationStore>{this.props.children}</LocationStore>;
    }
}

export default Router;
