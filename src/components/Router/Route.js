import React from "react";

import { LocationContext } from "./";

function Route(props) {
    return (
        <LocationContext.Consumer>
            {location => {
                return (
                    <React.Fragment>
                        {props.path === location.path && props.children}
                    </React.Fragment>
                );
            }}
        </LocationContext.Consumer>
    );
}

export default Route;
