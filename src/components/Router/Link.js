import React from "react";

import { LocationContext } from "./";

function Link(props) {
    return (
        <LocationContext.Consumer>
            {location => {
                return (
                    <a
                        href={props.to}
                        onClick={event => location.navigate(event, props.to)}
                    >
                        {props.children}
                    </a>
                );
            }}
        </LocationContext.Consumer>
    );
}

export default Link;
