import React from "react";

import { Link, LocationContext } from "./";

function NavLink(props) {
    return (
        <LocationContext.Consumer>
            {location => {
                if (props.to === location.path) {
                    return <span>{props.children}</span>;
                } else {
                    return <Link {...props}>{props.children}</Link>;
                }
            }}
        </LocationContext.Consumer>
    );
}

export default NavLink;
