import React from "react";

export const LocationContext = React.createContext();

export class LocationStore extends React.Component {
    state = {
        path: window.location.pathname
    };

    componentDidMount() {
        window.onpopstate = _ => {
            this.setState({ path: window.location.pathname });
        };
    }

    navigate(event, to) {
        event.preventDefault();
        window.history.pushState(null, null, to);

        this.setState({ path: to });
    }

    render() {
        return (
            <LocationContext.Provider
                value={{
                    path: this.state.path,
                    navigate: this.navigate.bind(this)
                }}
            >
                {this.props.children}
            </LocationContext.Provider>
        );
    }
}
