import React from "react";

import { NavLink } from "../Router";

function Menu(props) {
    return (
        <nav>
            <NavLink to="/">Dashboard</NavLink>
            <NavLink to="/scroll-to-example">Scroll to example</NavLink>
        </nav>
    );
}

export default Menu;
