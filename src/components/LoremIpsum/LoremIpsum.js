import React from "react";

function LoremIpsum() {
    return (
        <React.Fragment>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                libero velit, egestas ut lectus ac, semper viverra magna.
                Maecenas dignissim magna vel quam porta tincidunt. Nam rhoncus
                metus vel suscipit euismod. Nullam mollis felis in nisi molestie
                commodo. Suspendisse potenti. Suspendisse at ante vel tortor
                lacinia fermentum congue eget velit. Sed sit amet sapien sit
                amet arcu ullamcorper pulvinar.
            </p>
            <p>
                Sed vitae justo risus. Praesent tincidunt, metus ut dignissim
                iaculis, ligula ligula malesuada turpis, id lobortis felis dui
                sit amet velit. Duis luctus ex in felis consectetur dignissim.
                Cras id quam et enim euismod vehicula. Vestibulum ante ipsum
                primis in faucibus orci luctus et ultrices posuere cubilia
                Curae; Aliquam non neque quis eros pretium tristique in id
                neque. Proin congue ligula vitae rhoncus aliquam. Integer
                suscipit facilisis ex, et sodales ante interdum vitae. Aenean
                efficitur feugiat sapien vitae dictum. Quisque varius sapien sed
                quam dictum varius. Quisque ullamcorper urna augue, nec bibendum
                eros pellentesque at.
            </p>
            <p>
                Duis vitae ullamcorper purus. Vivamus vel nisl odio. Etiam
                sollicitudin vitae odio nec scelerisque. Sed fringilla, nunc vel
                venenatis suscipit, elit nunc gravida ex, vel maximus sapien
                sapien et risus. Vestibulum mollis nulla nec tortor tempor,
                pellentesque volutpat massa viverra. In hendrerit accumsan
                efficitur. Donec eleifend, nunc sit amet lacinia commodo, nunc
                orci aliquam ligula, non ultricies velit libero sollicitudin
                tellus. Cras venenatis turpis volutpat lacus blandit iaculis.
                Sed pretium mi non elit placerat viverra. Nunc ut enim non erat
                eleifend ultrices. Donec eu ante in dolor fermentum congue a non
                neque. Integer vel sagittis purus. Vestibulum vitae gravida
                dolor. Aenean ut nulla mauris. Etiam elementum placerat
                pharetra. Quisque vehicula magna in dui maximus, et ultricies
                odio convallis.
            </p>
            <p>
                Maecenas in ultrices neque, id convallis nunc. Proin vel odio eu
                nisi varius ornare non blandit mi. Nunc sit amet placerat est,
                sed mattis mi. Nullam congue venenatis sem vitae vestibulum.
                Curabitur nisi justo, tincidunt a vulputate sed, tempor at odio.
                Aliquam sit amet porttitor ipsum. Aliquam bibendum eget erat eu
                aliquet. Vivamus vulputate mi tellus, non eleifend sapien
                elementum a. Nullam eu justo in velit pretium congue. Duis
                tempor imperdiet dolor vel elementum. Ut risus felis, rutrum
                eget nunc euismod, lacinia lacinia velit. Cras nisi libero,
                placerat vitae vulputate ac, facilisis sed mauris. Aenean
                efficitur tortor non odio consectetur luctus. Curabitur
                tincidunt posuere scelerisque.
            </p>
            <p>
                Interdum et malesuada fames ac ante ipsum primis in faucibus.
                Cras efficitur purus sit amet enim feugiat, vitae rhoncus magna
                consequat. Fusce laoreet magna eu purus condimentum, nec
                pharetra elit vulputate. Duis fermentum erat quis nisl porta,
                vitae facilisis tellus commodo. Pellentesque suscipit augue
                ipsum, sit amet tempus turpis volutpat vel. Proin nec arcu
                lectus. Sed nec arcu at urna tempus tempor. Donec dictum mattis
                volutpat.
            </p>
            <p>
                Quisque ac augue tellus. In hac habitasse platea dictumst.
                Mauris bibendum luctus mi id vestibulum. Pellentesque cursus
                risus a augue varius, nec faucibus dui volutpat. Nullam non
                pretium tellus. Pellentesque habitant morbi tristique senectus
                et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor
                sit amet, consectetur adipiscing elit. Vestibulum sed ex
                lacinia, imperdiet ipsum nec, blandit metus.
            </p>
            <p>
                Suspendisse eget feugiat diam, quis cursus orci. Phasellus ac
                metus id turpis vulputate efficitur et et tortor. Pellentesque
                nec malesuada nisl, vel pharetra libero. Etiam vel magna arcu.
                Phasellus eu tortor nec eros ultrices vulputate eget eu massa.
                Integer pharetra quam lorem, vel luctus neque luctus ut. Fusce
                eget ipsum feugiat, venenatis nisl vel, aliquet orci. Etiam
                tempor, enim et convallis sodales, augue dolor pellentesque
                ante, ut interdum massa libero sit amet neque. Suspendisse
                maximus ornare urna, in rutrum orci varius vitae. Duis sit amet
                augue nec felis tincidunt malesuada ut et erat. Sed turpis
                lacus, maximus vel augue ac, placerat volutpat tellus. Curabitur
                eleifend finibus lacus vel lacinia. Etiam ut mauris malesuada,
                mollis lacus ac, pharetra mi.
            </p>
            <p>
                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                posuere cubilia Curae; Phasellus erat dolor, malesuada nec ipsum
                eget, auctor porttitor est. Etiam sapien magna, tincidunt sed
                massa id, lobortis consequat lorem. Morbi rhoncus est vitae
                turpis posuere bibendum. Nulla facilisi. Nam gravida posuere
                scelerisque. Vivamus vel libero elementum, tempor ligula
                pulvinar, consectetur sem. Nam congue ipsum et congue malesuada.
                Praesent quis nunc est. Nulla bibendum odio in sodales egestas.
            </p>
            <p>
                Nunc ut lorem metus. Quisque eleifend odio massa, vel feugiat
                lorem eleifend feugiat. Cras eu ligula odio. Nullam viverra
                massa ac augue tempus, ut dignissim justo congue. Phasellus nec
                mauris rhoncus, elementum nisl ut, faucibus arcu. Proin
                ullamcorper laoreet laoreet. Vivamus consequat neque neque, a
                faucibus nisi luctus et. Cras et ligula nec erat fringilla
                consequat. Aliquam vel tellus porttitor, pulvinar nunc quis,
                interdum dolor. Duis at pulvinar nisi. Nulla quis arcu cursus,
                vehicula nisi eu, elementum est. In ac leo eget tortor malesuada
                consectetur. Nulla facilisi. Curabitur porta, leo facilisis
                sagittis consectetur, felis augue posuere risus, ut vulputate
                libero ligula nec est.
            </p>
            <p>
                Pellentesque id sapien vel augue finibus molestie a sed nibh.
                Cras varius mollis ipsum et congue. Nunc sem nisl, vestibulum
                vitae ipsum eu, dictum tincidunt magna. Lorem ipsum dolor sit
                amet, consectetur adipiscing elit. Aenean non lacinia tortor,
                non bibendum orci. Pellentesque dapibus elit vitae suscipit
                malesuada. Praesent elementum commodo lorem vitae tincidunt. Sed
                ornare tortor eget turpis finibus dapibus. Curabitur vulputate
                nunc sit amet dolor bibendum auctor. Ut ultricies, ex et
                vestibulum aliquet, orci elit pretium ligula, id commodo sapien
                sapien quis enim.
            </p>
        </React.Fragment>
    );
}

export default LoremIpsum;
