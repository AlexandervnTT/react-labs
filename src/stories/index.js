import React from "react";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import { Button, Welcome } from "@storybook/react/demo";

import { Text } from "../components";

storiesOf("Storybook/Welcome", module).add("to Storybook", () => (
    <Welcome showApp={linkTo("Button")} />
));

storiesOf("Storybook/Button", module)
    .add("with text", () => (
        <Button onClick={action("clicked")}>Hello Button</Button>
    ))
    .add("with some emoji", () => (
        <Button onClick={action("clicked")}>
            <span role="img" aria-label="so cool">
                😀 😎 👍 💯
            </span>
        </Button>
    ));

storiesOf("Components/Text", module).add("with text", () => (
    <Text>Some text</Text>
));
